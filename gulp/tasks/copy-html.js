'use strict';

module.exports = function() {
  $.gulp.task('copy-html', function() {
    return $.gulp.src('./src/index.html').pipe($.gulp.dest($.config.root));
  });
};
