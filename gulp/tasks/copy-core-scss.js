'use strict';

module.exports = function() {
  $.gulp.task('copy-core-scss', function() {
    return $.gulp.src('./src/style/core/*').pipe($.gulp.dest($.config.root));
  });
};
