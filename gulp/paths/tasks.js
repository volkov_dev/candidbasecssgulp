'use strict';

module.exports = [
  './gulp/tasks/sass.js',
  './gulp/tasks/clean.js',
  './gulp/tasks/copy-core-scss',
  './gulp/tasks/copy-html',
  './gulp/tasks/watch.js',
  './gulp/tasks/server.js'
];